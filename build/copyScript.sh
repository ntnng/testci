#!/bin/bash
git diff $START_COMMIT $END_COMMIT --name-only --diff-filter=ACM | while read -r a ; do 
    case $a in
        *"classes"* | *"triggers"* | *"components"* | *"pages"* )
            rsync -R $a ./temp/;
            rsync -R "$a-meta.xml" ./temp/;;
        *"aura"* | *"lwc"* )
            rsync -R $a ./temp/;;
        *)
            rsync -R $a ./temp/;;
    esac
done