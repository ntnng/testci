/**
 *  Apttus Billing 
 *  InvoiceRunConstants
 *   
 *  @2014-2018 Apttus Inc. All rights reserved.
 */

public with sharing class InvoiceRunConstants {
	// sobject name
	public static final String SOBJECT_NAME = 'InvoiceRun__c';
	

	//Default End Date
	public static final String DEFAULT_INVOICE_RUN_END_DATE = '2099-12-31';
}