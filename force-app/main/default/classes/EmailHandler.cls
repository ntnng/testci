global class EmailHandler implements Messaging.InboundEmailHandler {
      global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
          if (email <> null) {
              Email__c receivedEmail = new Email__c(
                  From__c = email.fromAddress,
                  HtmlBody__c = email.htmlBody,
                  PlainBody__c = email.plainTextBody,
                  Subject__c = email.subject,
                  CCAddresses__c = email.ccAddresses != null ? String.join(email.ccAddresses, ',') : null
              );
              insert receivedEmail;
              if (email.binaryAttachments != null && email.binaryAttachments.size() > 0 ) {
                  list<Attachment> attachments = new list<Attachment>();
                  for (Integer i = 0; i < email.binaryAttachments.size(); i++) {
                      attachments.add(
                          new Attachment(ParentId = receivedEmail.Id, 
                                         isPrivate = false, 
                                         Name = email.binaryAttachments[i].fileName, 
                                         Body = email.binaryAttachments[i].body)
                      );
                  }
                  if (attachments.size() > 0) {
                      insert attachments;
                  }
              }
              if (email.textAttachments != null && email.textAttachments.size() > 0 ) {
                  list<Note> notes = new list<Note>();
                  for (Integer i = 0; i < email.textAttachments.size(); i++) {
                      notes.add(
                          new Note(ParentId = receivedEmail.Id, 
                                   isPrivate = false, 
                                   Title = email.textAttachments[i].fileName, 
                                   Body = email.textAttachments[i].body)
                      );
                  }
                  if (notes.size() > 0) {
                      insert notes;
                  }
              }
          }
          system.debug(email);
          system.debug(envelope);
          Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
          result.message='Received email and this is the only change';
          return result;
      }
  }