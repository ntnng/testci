@isTest
private class EmailHandler_Test {
    static testMethod void testEmail() {
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'Test Job Applicant';
        email.fromname = 'FirstName LastName';
        env.fromAddress = 'someaddress@email.com';
        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfile.pdf';
        attachment.mimeTypeSubType = 'application/pdf';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        
        Messaging.InboundEmail.TextAttachment attachment2 = new Messaging.InboundEmail.TextAttachment();
        attachment2.body = 'my attachment text';
        attachment2.fileName = 'textfile.txt';
        attachment2.mimeTypeSubType = 'text/plain';
        email.textAttachments = new Messaging.inboundEmail.TextAttachment[] { attachment2 };

        EmailHandler handler = new EmailHandler();
        handler.handleInboundEmail(email, env);
        
        list<Email__c> em = [Select Id From Email__c limit 1];
        System.assertEquals(1, em.size());

        Attachment a = [select name from attachment where parentId = :em[0].id];
        System.assertEquals(a.name,'textfile.pdf');
        Note na = [select title from note where parentId = :em[0].id];
        System.assertEquals(na.title,'textfile.txt');

    }
}