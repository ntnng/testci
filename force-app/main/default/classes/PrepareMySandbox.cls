global class PrepareMySandbox implements SandboxPostCopy {
    global void runApexClass(SandboxContext context) {
        System.debug('Org ID: ' + context.organizationId());
        System.debug('Sandbox ID: ' + context.sandboxId());
        System.debug('Sandbox Name: ' + context.sandboxName());
        System.enqueueJob(new PrepareSandbox() );
     }
     
     public class PrepareSandbox implements Queueable {
         public void execute(QueueableContext context) {
             User u = [SELECT Id FROM User WHERE FirstName Like '%Nitin%' LIMIT 1];
             System.debug('Queueable called ');
             insert new FeedItem(ParentId = u.Id, Body = 'PrepareSandbox  Queueable called');
             ntnng__c mc = ntnng__c.getOrgDefaults();
             insert new FeedItem(ParentId = u.Id, Body = ' ' + mc );
             System.debug('mc ' + mc);
             if (mc <> null && mc.CreateData__c) {
                 insert new FeedItem(ParentId = u.Id, Body = 'Sandbox refreshed');
             }
         }
     }
}